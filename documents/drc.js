var message="Right-mouse click has been disabled.";
function click(e)
{
  if (document.all)
  {
    if (event.button==2||event.button==3)
    {
      alert(message);
      return false;
    }
  }
  else
  {
    if (e.button==2||e.button==3)
    {
      e.preventDefault();
      e.stopPropagation();
      alert(message);
      return false;
    }
  }
}

if (document.all) // for IE
{
  document.onmousedown=click;
}
else // for FF
{
  document.onclick=click;
}